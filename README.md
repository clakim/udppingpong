PingClient, MPCS Networks 54001

This is a UDP-based ping client that communicates with a provided UDP-based ping server.
Its functionality emulates standard ping utility functionality in modern OS's, using UDP rather than ICMP.  

This ping protocol:  
1. enables a client machine to send a UDP packet of data to a remote server machine, and have the remote server machine echo the data back  
2. lets hosts determine round-trip times and packet loss to other machines
