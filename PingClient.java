/**
 * PING CLIENT
 * PingClient.java
 * A UDP-based ping client that communicates with UDP-based ping server.
 * @author Claire Kim
 * @version 1.0
 */

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;


public class PingClient
{
    public static void main(String[] args) throws Exception
    {
        // Initializes command line argument
        int server_port = -1;                     // server port
        InetAddress server_IPaddress = null;      // server address
        int count = 0;                            // number of pings
        int period = 1000;                        // period of pings
        int timeout = 1000;                       // socket timeout

        // Initializes synchronized array lists accessible by multiple threads
        List<Integer> elapsedTimesList = Collections.synchronizedList(new ArrayList<Integer>());
        List<Long> sendTimeStampList = Collections.synchronizedList(new ArrayList<Long>());
        List<Long> returnTimeStampList = Collections.synchronizedList(new ArrayList<Long>());

        // Processes command-line arguments.
        for (String arg : args) {
            String[] splitArg = arg.split("=");
            if (splitArg.length == 2 && splitArg[0].equals("--server_ip")) {
                server_IPaddress =  InetAddress.getByName(splitArg[1]);
            } else if (splitArg.length == 2 && splitArg[0].equals("--server_port")) {
                server_port = Integer.parseInt(splitArg[1]);
            } else if (splitArg.length == 2 && splitArg[0].equals("--count")) {
                count = Integer.parseInt(splitArg[1]);
            } else if (splitArg.length == 2 && splitArg[0].equals("--period")) {
                period = Integer.parseInt(splitArg[1]);
            } else if (splitArg.length == 2 && splitArg[0].equals("--timeout")) {
                timeout = Integer.parseInt(splitArg[1]);
            } else {
                System.err.println("Usage: java PingClient --server_ip=<server_IPaddress> --server_port=<server_port> " +
                        "--count=<count> --period=<period> --timeout=<timeout>");
                return;
            }
        }

        // Checks that port is accessible
        try {
            if (server_port == -1) {
                System.err.println("Must specify port number with --port");
                return;
            }
            if (server_port <= 1024) {
                System.err.println("Avoid potentially reserved port number: " + server_port + " (should be > 1024)");
                return;
            }
        } catch (Exception e) {
            System.out.println("Invalid port");
            return;
        }

        // Checks that period is a positive value
        if (period <= 0){
            System.err.println("Period must > 0 for the intents of this ping implementation");
            return;
        }


        // Prints the address to which the pings are to be sent
        System.out.println("PING " + server_IPaddress.getHostAddress());

        // Instantiates a thread pool of fixed number, where one thread manages one ping
        ScheduledThreadPoolExecutor threadPool = new ScheduledThreadPoolExecutor(count);

        // For each ping, schedules one-time instantiation of SendPingTask at specified time, which is dependent on period
                // This specified time is referred to as "delay" parameter in the ScheduledThreadPoolExecutor documentation.
        for (int i = 0; i < count; i++) {
            threadPool.schedule(new SendPingTask(server_IPaddress, server_port, count, threadPool, timeout, i, period,
                    elapsedTimesList, sendTimeStampList, returnTimeStampList), period * i, TimeUnit.MILLISECONDS);
        }
    }


    /**
     * Static inner class SendPingTask manages sending and receiving of pings
     */
    public static class SendPingTask implements Runnable {
        InetAddress address;
        int port;
        int count;
        ScheduledThreadPoolExecutor threadPool;
        int timeout;
        int counter;
        int period;
        List<Integer> elapsedTimesList;
        List<Long> sendTimeStampList;
        List<Long> returnTimeStampList;

        long current_pingsent_time = 0;


        /**
         * Constructor for SendPingTask
         * @param address server address
         * @param port server port
         * @param count number of pings
         * @param threadPool threadPool containing one thread per ping
         * @param timeout socket timeout value
         * @param counter counter
         * @param period period of ping sending
         * @param elapsedTimesList ArrayList that stores calculated RTTs
         * @param sendTimeStampList ArrayList stores timestamps of sent pings
         * @param returnTimeStampList ArrayList stores timestamps of received pings
         */
        public SendPingTask(InetAddress address, int port, int count, ScheduledThreadPoolExecutor threadPool, int timeout,
                            int counter, int period, List<Integer> elapsedTimesList, List<Long> sendTimeStampList,
                            List<Long> returnTimeStampList) {
            try {
                this.address = address;
                this.port = port;
                this.count = count;
                this.threadPool = threadPool;
                this.timeout = timeout;
                this.counter = counter;
                this.period = period;
                this.elapsedTimesList = elapsedTimesList;
                this.sendTimeStampList = sendTimeStampList;
                this.returnTimeStampList = returnTimeStampList;
            }

            catch (Exception e) {
                System.err.println("Error setting up SendPingTask: " + e.getMessage());
            }
        }

        /**
         * @method run - Run method launches the sending and receiving of ping data
         */
        public void run() {

            // Instantiates socket at randomly available port for this run
            DatagramSocket socket = null;
            try {
                socket = new DatagramSocket();
            } catch (SocketException e) {
                System.err.println("Error with instantiating datagram socket: " + e.getMessage());
            }

            // Sets current timeout to the timeout indicated in command line arg
            try {
                socket.setSoTimeout(timeout);
            } catch (SocketException e) {
                System.err.println("Error with setting socket timeout: " + e.getMessage());
            }

            // Saves the timestamp when ping associated with this run is sent
            current_pingsent_time = System.currentTimeMillis();
            sendTimeStampList.add(current_pingsent_time);

            // Specifies PING string to add to data buffer
            String data = "PING " + counter + " " + current_pingsent_time + "\r\n";

            // Instantiates buffer array to collect the ping-sending data
            byte[] buf;
            buf = data.getBytes();

            // Creates a datagram packet to hold the sending data
            DatagramPacket sendingPacket = new DatagramPacket(buf, buf.length, address, port);

            // Sends packet
            try {
                socket.send(sendingPacket);
            }
            catch (IOException e) {
                System.err.println("Error with pinging: " + e.getMessage());
            }

            // Creates a datagram packet to receive the incoming data
            byte[] buf2 = new byte[1024];
            DatagramPacket receivingPacket = new DatagramPacket(buf2, buf2.length);

            // Receives packet
            try {
                socket.receive(receivingPacket);

                // Calculates the elapsed time
                int elapsedTime = (int)(System.currentTimeMillis() - current_pingsent_time);

                System.out.println("PONG " + receivingPacket.getAddress().getHostAddress() + ": seq=" + counter + " time="
                        + elapsedTime + " ms");

                // Saves the RTT associated with this ping
                elapsedTimesList.add(elapsedTime);
                returnTimeStampList.add(System.currentTimeMillis());
            }

            // Handles exception when packet late (arrives past timeout), or packet dropped by server
            catch (SocketTimeoutException e) {
                returnTimeStampList.add(System.currentTimeMillis());
            }

            catch (IOException e) {
                e.printStackTrace();
            }

            counter++;

            // Calls method to print statistics and closes thread pool as soon as all PING "responses" received.
            if (returnTimeStampList.size() == count){
                    printStatSummary();
                    threadPool.shutdown();
                }
        }


        /**
         * @method printStatSummary() - prints statistics summary
         */
        public void printStatSummary() {

            // Prints summary header line
            System.out.println("\n--- " + address.getHostAddress() + " ping statistics ---");

            // Statistical variables
            int min;
            int max;
            double loss;
            double avg;
            long cumulative_time;

            // Sorts array lists from smallest to greatest value
            Collections.sort(elapsedTimesList);
            Collections.sort(sendTimeStampList);
            Collections.sort(returnTimeStampList);

            // Computes cumulative time between first PING and last response
            cumulative_time = returnTimeStampList.get(returnTimeStampList.size() - 1) - sendTimeStampList.get(0);

            // Sets statistical variables in case of pos. # of packets returned
            if (elapsedTimesList.size() != 0) {
                min = elapsedTimesList.get(0);
                max = elapsedTimesList.get(elapsedTimesList.size() - 1);

                // Computes packet loss
                loss = 100 * (count - elapsedTimesList.size()) / (double) count;

                // Computes packet average
                int sum = 0;
                for (int i = 0; i < elapsedTimesList.size(); i++) {
                    sum += elapsedTimesList.get(i);
                }
                avg = ((double)sum / elapsedTimesList.size());
            }

            // Sets statistical variables in case of 0 packets returned
            else {
                min = 0;
                max = 0;
                loss = 100;
                avg = 0;
            }

            // Prints aggregate summary of packets
            System.out.println(count + " packets transmitted, " + elapsedTimesList.size() + " received, "
                    + (int)Math.round(loss) + "% packet loss, time " + cumulative_time + " ms");

            // Prints individual RTT summary of packets
            System.out.println("rtt min/avg/max = " + min + "/" + (int) Math.round(avg) + "/" + max);
        }
    }
}